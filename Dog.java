/**
 * Created with IntelliJ IDEA.
 * User: vishu
 * Date: 11/9/13
 * Time: 11:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class Dog extends Animal {

//    The default behavior of java is to give a constructor if not specified
//    and the constructor calls the super class constructor with no agruments
//    public Dog(){
//        super();
//    }

    @Override
    public String toString(){
        return "BARK";
    }

}


