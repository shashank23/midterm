/**
 * Created with IntelliJ IDEA.
 * User: vishu
 * Date: 10/31/13
 * Time: 8:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class Driver {

    public static void main(String[] args) {


//        For heap vs stack
//        http://stackoverflow.com/questions/6801676/heap-vs-stack-vs-perm-space


//        Scope of a variable inside try catch
//        if declared inside try /catch then you cannot access it
//        outside the try block, if you uncomment the block below
//        name is inaccessible outside of try
      /*  try {
            String name = "HELLO";
        } catch (Exception e) {

        }
        System.out.println(name);
      */


//        Allowed things
        Animal a = new Dog();
        System.out.println(a.toString());     // Will print bark

        Dog d = (Dog) a;
        System.out.println(d.toString());     // Will print bark

        Dog dg = new Dog();
        Animal na = (Animal) dg;
        System.out.println(na.toString());   // Will print bark


//        na = (dog) null;
//        System.out.println(na.toString());  // This will throw a run time exception, Null Pointer exception, but it will compile

//        Not Allowed
//        Dog dg3 = new Animal();


//        Not Allowed, throws ClassCastException, but will compile
//        Animal dg1 = (Cat) a;


//       this will also throw ClassCastException, but will compile
//        Animal myanimal = new animal();
//        Cat mycat = (cat) myanimal;
//        System.out.println(myanimal.toString());

//        Operators on String can do +=, -=
        String name = "BOB";
        name += "SALAMANDER";

//        name -= "TIM"; Cannot do -=


//        Equality operator comparing references
        Animal aa = new Animal();
        Animal bb = new Animal();

        if (aa == bb)
            System.out.println("Animals are equal");

        String name_bob = "bob";
        String name_adam = "adam";

        if (name_bob == name_adam)
            System.out.println("Names are equal");

//      Java primitive types
//       http://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html

    }
}